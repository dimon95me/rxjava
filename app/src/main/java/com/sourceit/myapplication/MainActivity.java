package com.sourceit.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    TextView text;
    int count;

    Subscription s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);

        text.setOnClickListener(v -> {
            if (s==null){
                s = Observable.interval(1, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                text.setText("Tick" + String.valueOf(count));
                                count++;
                            }
                        });
            }
            else{
                s.unsubscribe();
                s=null;
            }

        });

//        Observable.just("Hello world")
////                .map(new Func1<String, String>() {
////                    @Override
////                    public String call(String s) {
////                        s.replace(" ", "!") + "!")
////                       .subscribe(s -> text.setText(s));
////                    }
////                }


    }

    @Override
    protected void onStop() {
        super.onStop();
        s.unsubscribe();
    }
}
